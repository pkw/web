PROJ=	${.CURDIR:T}
CLROOT=	$(HOME)/common-lisp
WWWROOT=	/var/www/htdocs

.PHONY: link clean-cache deploy undeploy deploy-static clean

link:
	rm -rf $(CLROOT)/${PROJ}
	ln -s $(.CURDIR) $(CLROOT)/$(PROJ) 

# the repl command; asdf:clear-source-registry is probably better
clean-cache:
	rm -rf $(HOME)/.cache/common-lisp

deploy-static:
	mkdir -p $(WWWROOT)/static
	cp -r _static/* $(WWWROOT)/static/
	cp _static/favicon.ico $(WWWROOT)

clean:
	rm -f a.out

