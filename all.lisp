(uiop:define-package :web/all
  (:nicknames :web)
  (:mix-reexport #:web/web
                 #:web/template
                 #:web/view
                 #:web/route
                 #:web/serve
                 #:web/util

                 #:web/urt
                 #:web/urt/view

                 #:web/time
                 #:web/dev1
                 #:web/thread-test))

