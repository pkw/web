;;request class to abstract over a request in
;;different environemnts.  The first one is
;;using a fastcgi/cffi based request.
(uiop:define-package :web/request
  (:mix :cl)
  (:export #:request
           #:make-request
           #:r1))

(in-package :web/request)

(defclass request ()
  ((path-info
     :initform nil
     :accessor path-info)))

(defun make-request (cffi-req)
  (make-instance 'request))

(defun r1 ()
  (let ((r (make-request '())))
    (format t "r:~a~%" r)
    (setf (web/request::path-info r) "/asd/v1/")
    r))

#|
(defgeneric greet (obj)
  (:documentation "say hello"))
;; STYLE-WARNING: redefining COMMON-LISP-USER::GREET in DEFGENERIC
;; #<STANDARD-GENERIC-FUNCTION GREET (2)>

(defmethod greet ((obj person))
  (format t "Hello ~a !~&" (name obj)))
;; #<STANDARD-METHOD GREET (PERSON) {1007C26743}>

(greet p1) ;; => "Hello me !"
(greet c1) ;; => "Hello Alice !"

(defmethod greet ((obj child))
  (format t "ur so cute~&"))
;; #<STANDARD-METHOD GREET (CHILD) {1008F3C1C3}>

(greet p1) ;; => "Hello me !"
(greet c1) ;; => "ur so cute"
|#
