(uiop:define-package :web/dev1
  (:mix :cl)
  (:export #:test-dj))

(in-package :web/dev1)

(defun test-dj ()
  (format t "test-dj~%")
  (let ((data '(:var1 "+++var1 ..." 
                :var2 "***var2 ...")))
    (web/template:render "test-dj.html" data)))


#|
Function: (url-encodestring)
(web:parse-form-vars-1 "asd=123&qwe=999&ddd=&fff=hddhdh&g=")
|#
