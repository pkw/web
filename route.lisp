;; Setup a hashmap of PATH_INFO entries to render functions.
(uiop:define-package :web/route
  (:mix :cl :web/view :web/urt/view)
  (:export #:route
           #:*routes*))

(in-package :web/route)

(defparameter *routes* (make-hash-table :test 'equal))
(setf (gethash "" *routes*) #'web/view:main)
(setf (gethash "/asd/v1/" *routes*) #'web/view:v1)
(setf (gethash "/form-test/" *routes*) #'web/view:form-test)
(setf (gethash "/urt/servers/" *routes*) #'web/urt/view:list-servers)

(defun route (path-info)
  (let ((view (gethash path-info *routes*)))
    (format t "view:~a~%" view)
    (if (null view)
      (setf view #'web/view:default-view))
    view))



