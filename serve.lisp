(uiop:define-package :web/serve
  (:mix :cl :cffi :web/web :web/route)
  (:export #:serve))

(in-package :web/serve)

(defun serve ()
  (fcgx-init)
  (let ((req (foreign-alloc :char :count (myfcgi-request-type-size)))
        (sockfd (fcgx-opensocket ":8000" 1024)))
	  (fcgx-initrequest req sockfd 0)

    (loop while (= 0 (fcgx-accept-r req))
          do (format t "req:~a~%" req)

             (let* ((path-info (myfcgi-get-param req "PATH_INFO"))
                    (v (web/route:route path-info)))
               (format t "PATH_INFO:~a v:~a~%" path-info v)

               ;; response 
               (myfcgi-text-html-header req)
               (fcgx-fprintf (myfcgi-request-out req) (funcall v req)))

             (fcgx-finish req))

    (fcgx-free req sockfd)
    (foreign-free req)))

