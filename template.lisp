(uiop:define-package :web/template
  (:mix :cl :djula)
  (:export #:*templates*
           #:render))

(in-package :web/template)

(djula:add-template-directory (asdf:system-relative-pathname "web" "_templates/"))

(defparameter *templates* (make-hash-table :test 'equal)) ;; equalp case-insens

(defun get-template (name)
  (format t "()()() get-template :name:~a~%" name)
  (if (not (gethash name *templates*))
    (let ((tmpl (djula:compile-template* name)))
      (setf (gethash name *templates*) tmpl)
      (format t "sethash *templates* name:~a~%" name)))
  (gethash name *templates*))

(defun render (tmpl-name data)
  (let ((tmpl (get-template tmpl-name)))
    ;;(format t "tmpl-name:~a data:~a~%" tmpl-name data)
    (djula:render-template* tmpl nil :data data)))

