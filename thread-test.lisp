(uiop:define-package :web/thread-test
  (:mix :cl #:bordeaux-threads)
  (:export #:join-destroy-thread))

(in-package :web/thread-test)

(defmacro my-until (condition &body body)
	(let ((block-name (gensym)))
		`(block ,block-name
			 (loop
					 (if ,condition
							 (return-from ,block-name nil)
							 (progn
									 ,@body))))))

(defun join-destroy-thread ()
	(let* ((s *standard-output*)
				(joiner-thread (bt:make-thread
												(lambda ()
													(loop for i from 1 to 10
														 do
															 (format s "~%[Joiner Thread]  Working...")
															 (sleep (* 0.01 (random 100)))))))
				(destroyer-thread (bt:make-thread
													 (lambda ()
														 (loop for i from 1 to 1000000
																do
																	(format s "~%[Destroyer Thread] Working...")
																	(sleep (* 0.01 (random 10000))))))))
		(format t "~%[Main Thread] Waiting on joiner thread...")
		(bt:join-thread joiner-thread)
		(format t "~%[Main Thread] Done waiting on joiner thread")
		(if (bt:thread-alive-p destroyer-thread)
				(progn
					(format t "~%[Main Thread] Destroyer thread alive... killing it")
					(bt:destroy-thread destroyer-thread))
				(format t "~%[Main Thread] Destroyer thread is already dead"))
		(my-until (bt:thread-alive-p destroyer-thread)
					 (format t "[Main Thread] Waiting for destroyer thread to die..."))
		(format t "~%[Main Thread] Destroyer thread dead")
		(format t "~%[Main Thread] Adios!~%")))



