(uiop:define-package :web/urt
  (:mix :cl :urt)
  (:export #:urt-test))

(in-package :web/urt)

(defun urt-test ()
  (format t "urt-test...~%")
  (let ((rr (urt:get-raw-response)))
    (format t "rr:~a~%" rr)))
