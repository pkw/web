(uiop:define-package :web/urt/view
  (:mix :cl 
        :web/web 
        :web/template 
        :urt)
  (:export #:list-servers))

(in-package :web/urt/view)

(defun list-servers (req) 
  (let* ((data)
         (rr (urt:get-raw-response))
         (servers (urt:parse-servers rr)))
    (push servers data)
    (push :servers data)
    (render "urt/servers.html" data)))

