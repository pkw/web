(uiop:define-package :web/util
  (:mix :cl :alexandria :str)
  (:export #:parse-form-vars))

(in-package :web/util)

;; Return hash-table with string keys, and nil value
;; where the var is set but has no value.
;;   TODO: htmldecode things like + as space...
;;    djula has: Function: (url-encodestring)
(defun parse-form-vars (vars-str)
  (let ((l (split "&" vars-str))
        (ht (make-hash-table :test 'equalp)))
    (dolist (i l)
      (format t "---i:~a~%" i)
      (let* ((ss (split "=" i :omit-nulls t))
             (key (make-keyword (string-upcase (nth 0 ss)))))
        (format t "ss:~a~%" ss)
        (if (= 2 (length ss))
          (setf (gethash key ht) (nth 1 ss))
          (setf (gethash key ht) nil))))
    ht))

