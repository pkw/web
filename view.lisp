(uiop:define-package :web/view
  (:mix :cl :alexandria :cffi :web/web :web/template :web/util)
  (:export #:main
           #:default-view
           #:v1
           #:form-test))

(in-package :web/view)

(defun default-view (req) 
  (render "default.html" '()))

(defun main (req) 
  (format t "web/view:main~%")
  (let* ((data '(:var1 "+++var1 ..." :var2 "***var2 ...")))
    (render "main.html" data)))

(defun v1 (req)
  (render "v1.html" '()))

(defun form-test (req) 
  (let ((request-method (myfcgi-get-param req "REQUEST_METHOD"))
        (data '()))
    (format t "request-method:~a~%" request-method)
    (format t "req:~a~%" req)

    (if (equalp "POST" request-method)
      (progn
        (format t "in POST...~%")
        (let* ((content-length (parse-integer (myfcgi-get-param req "CONTENT_LENGTH")))
               (buf (foreign-alloc :char :count content-length))
               (in (myfcgi-request-in req)))
          (format t "content-length:~a type-of:~a~%" content-length (type-of content-length))
          (format t "(empty)  buf:~a~%" buf)
          (fcgx-get-str buf content-length in)
          (format t "(filled) buf:~a~%" buf)
          (let* ((buf-str (foreign-string-to-lisp buf :count content-length))
                 (vars (web/util:parse-form-vars buf-str)))
            (format t "buf-str:~a~%" buf-str)
            (format t "vars:~s~%" (hash-table-plist vars))
            (setf data vars))
          (foreign-free buf)))
      (format t "NOT in POST...~%"))

    (render "form-test.html" data)))

