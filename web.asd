(defsystem "web"
  :class :package-inferred-system
  :serial t
  :depends-on (:cffi-libffi :web/all)) ;; <-- :urt can go here or in :mix (:urt .. of the package itself.

#|
To "load the cffi-libffi system", you need to specify it as
dependency in the library's .asd file. (Note: cffi-libffi needs the
C library libffi to be installed on your system.)
|#

(register-system-packages "web/all" '(:web))

