;;; web.lisp is the low-level code for writing web pages.
;;; It interops with libfcgi using cffi.
;;; It also extends it some, but still low level.
;;; (at a minimum keep all cffi calls here.)

(uiop:define-package :web/web
  (:mix :cl :cffi)
  (:export #:fcgx-init
           #:myfcgi-request-type-size
           #:myfcgi-get-param
           #:fcgx-opensocket
           #:fcgx-initrequest
           #:fcgx-free
           #:fcgx-accept-r
           #:fcgx-finish
           #:fcgx-fprintf
           #:fcgx-get-str
           #:myfcgi-request-out
           #:myfcgi-request-in
           #:myfcgi-text-html-header))

(in-package :web/web)

(pushnew
  (format nil "~a/common-lisp/web/" (uiop:getenv "HOME"))
  cffi:*foreign-library-directories* 
  :test #'equal)

(define-foreign-library fcgi 
  (:unix "/usr/local/lib/libfcgi.so.0.0")
  (t (:default "/usr/local/lib/libfcgi")))

(use-foreign-library fcgi)

(define-foreign-library myfcgi 
  (:unix "myfcgi.so") ;; :default works too, but this is more accurrate for now.
  (t (:default "myfcgi")))

(use-foreign-library myfcgi)

(defcfun ("myfcgi_request_size" myfcgi-request-type-size) :int
  "This returns the sizeof(FCGX_Request) for malloc stuff.") 

(defcfun "FCGX_Init" :void)

(defcfun "FCGX_OpenSocket" :int
  (s :string)
  (i :int))

(defcfun "FCGX_InitRequest" :void
  (p :pointer)
  (sockfd :int) 
  (i :int))

(defcfun "myfcgi_mainloop1" :void 
  (req :pointer))

(defcfun "FCGX_Free" :void
  (req :pointer)
  (sockfd :int))

(defcfun "FCGX_Accept_r" :int
  (req :pointer))

(defcfun "FCGX_Finish" :void
  (req :pointer))

(defcfun "myfcgi_get_param" :string
  "a bogus 'name' returns nil, a empty var like PATH_INFO returns a blank string"
  (req :pointer)
  (name :string))

(defcfun "FCGX_FPrintF" :void
  (out :pointer)
  (name :string))

(defcfun "myfcgi_request_out" :pointer
  "Return the 'out' field of the FCGX_Request struct."
  (req :pointer))

(defcfun "myfcgi_request_in" :pointer
  "Return the 'in' field of the FCGX_Request struct."
  (req :pointer))

(defcfun "myfcgi_text_html_header" :void
  (req :pointer))

;;FCGX_GetStr(ContentBuffer, ContentLength, Request.in);
;; buf gets the value
(defcfun ("FCGX_GetStr" fcgx-get-str) :void
  (buf :pointer)
  (content-length :int)
  (in :pointer))
